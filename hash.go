package simplytap

import (
	"crypto/md5"
	"crypto/sha1"
	"encoding/hex"
	"fmt"
	"io"
)

// Hash represents a Hashable string
type Hash struct {
	s string
}

// MD5 calculates the MD5 hash of a string
func (h Hash) MD5() string {
	m := md5.New()
	io.WriteString(m, h.s)
	return fmt.Sprintf("%x", m.Sum(nil))
}

// SHA1 calculates the SHA1 hash of a string
func (h Hash) SHA1() string {
	m := sha1.New()
	io.WriteString(m, h.s)
	return fmt.Sprintf("%x", m.Sum(nil))
}

// Encrypt calculates the Encript of a string
func (h Hash) Encrypt() string {
	data := h.s
	var encrypted string
	for i := 0; i < len(data); i++ {
		chr := data[i : i+1]
		hex := fmt.Sprintf("%x", chr)
		hex = hex[1:] + hex[0:1]
		encrypted = encrypted + hex
	}
	return encrypted
}

// Decrypt calculates the Encript of a string
func (h Hash) Decrypt() string {
	data := h.s
	var dencrypted string
	for i := 0; i < len(data)-1; i = i + 2 {
		hexValue := data[i : i+2]
		hexValue = hexValue[1:] + hexValue[0:1]
		chr, _ := hex.DecodeString(hexValue)
		dencrypted = dencrypted + fmt.Sprintf("%s", chr)
	}
	return dencrypted
}
