package simplytap

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

// Card represent the card
type Card struct {
	SimplyTap   SimplyTap `json:"-"`
	CardID      string    `json:"card_id,omitempty"`
	Command     string    `json:"command,omitempty"`
	CardBrand   string    `json:"card_brand,omitempty"`
	Key         string    `json:"consumer_key,omitempty"`
	Secret      string    `json:"consumer_secret,omitempty"`
	AccessToken string    `json:"access_token,omitempty"`
	TokenSecret string    `json:"token_secret,omitempty"`
}

// CardResponse represent the card response
type CardResponse struct {
	Data   DataResponse
	Status string `json:"status,omitempty"`
}

// NewCardResource initializes the User Resource
func NewCardResource(simplyTap SimplyTap) Card {
	return Card{
		SimplyTap: simplyTap,
	}
}

const (
	// CommandCreateCard Command to create a user
	CommandCreateCard = "CreateCardToken"

	// CommandDeleteCard Command to create a user
	CommandDeleteCard = "DeleteCard"
)

// Posible status values for user create and update
const (
	StatusNoCardBrandOrBadCredentials = "1"
	StatusBadAccessTokenSecret        = "2"
	StatusCardCreationError           = "3"

	StatusNoCardBrandOrBadCredentialsDescription = "No card brand or bad consumer credentials or bad access token data"
	StatusCardBrandRequiredDescription           = "Card brand is required"
	StatusBadAccessTokenSecretDescription        = "Bad access token/secret"
	StatusCardCreationErrorDescription           = "Card creation error"
	StatusRequiredTokenSecretDescription         = "Token Secret is required"
	StatusRequiredAccessTokenDescription         = "Access Token is required"
)

// Create create card
func (c *Card) Create() error {
	c.Command = CommandCreateCard

	c.CardBrand = c.SimplyTap.CardBrand
	c.Key = c.SimplyTap.Issuer.Key
	c.Secret = c.SimplyTap.Issuer.Secret

	if err := c.validateCreate(); err != nil {
		return err
	}

	return c.Send()
}

func (c *Card) validateCreate() error {
	if c.CardBrand == "" {
		return errors.New(StatusCardBrandRequiredDescription)
	}

	if c.Key == "" {
		return errors.New(StatusRequiredKeyDescription)
	}

	if c.Secret == "" {
		return errors.New(StatusRequiredPasswordDescription)
	}

	if c.TokenSecret == "" {
		return errors.New(StatusRequiredTokenSecretDescription)
	}

	if c.AccessToken == "" {
		return errors.New(StatusRequiredAccessTokenDescription)
	}

	return nil
}

// SetWalletTokens set access_token and others
func (c *Card) SetWalletTokens(wallet Wallet) error {
	c.AccessToken = wallet.AccessToken
	c.TokenSecret = wallet.TokenSecret
	return nil
}

// Delete unregister card
func (c *Card) Delete() error {
	card := NewCardResource(c.SimplyTap)
	card.Command = CommandDeleteCard
	card.AccessToken = c.AccessToken
	card.TokenSecret = c.TokenSecret
	return card.Send()
}

// Response get the description for the response code
func (c Card) Response(code string) string {
	switch code {
	case StatusNoCardBrandOrBadCredentials:
		return StatusNoCardBrandOrBadCredentialsDescription
	case StatusBadAccessTokenSecret:
		return StatusBadAccessTokenSecretDescription
	case StatusCardCreationError:
		return StatusCardCreationErrorDescription
	}
	return StatusUncaughtExceptionDescription
}

// Send send the request to simplytap
func (c *Card) Send() error {
	jsonString, _ := json.Marshal(c)
	data := fmt.Sprintf("DATA=%s", string(jsonString))
	_, body := c.SimplyTap.Send(http.MethodPost, c.SimplyTap.Host+"accounts/AdminApi", data, c.SimplyTap.Issuer.Key, c.SimplyTap.Issuer.Secret)

	response := CardResponse{}
	err := json.Unmarshal([]byte(string(body)), &response)

	if response.Data.AccessToken != "" && response.Data.TokenSecret != "" {
		c.AccessToken = response.Data.AccessToken
		c.TokenSecret = response.Data.TokenSecret
		c.CardID = response.Data.CardID
	}

	if err != nil {
		return err
	}

	if response.Status == StatusSuccess {
		return nil
	}

	return errors.New(c.Response(response.Status))
}
