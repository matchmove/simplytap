package simplytap

import (
	"fmt"
	"net/http"
	"reflect"
	"testing"

	"github.com/jarcoal/httpmock"
)

const (
	// CardCreateMockResponse card create mock response
	CardCreateMockResponse = `{
	"status":"0",
	"data":{
		"access_token":"access_token_mock_response",
		"spec_ver":"spec_ver_mock_response",
		"agent_hash":"agent_hash_mock_response",
		"card_id":"card_id_mock_response",
		"token_secret":"token_secret_mock_response"
	}
}`
)

var createdCard Card

func getCreatedCard() Card {
	return createdCard
}

func TestEncrypt(t *testing.T) {
	value := "thequickbrownfox"
	hashEncrypt := Hash{value}.Encrypt()

	hashDecrypt := Hash{hashEncrypt}.Decrypt()

	if value != hashDecrypt {
		t.Errorf("Expecting `%s`, Got `%s`", hashEncrypt, hashDecrypt)
	}
}

func TestNewCardResource(t *testing.T) {
	simplyTap := GetConfig()
	card := NewCardResource(simplyTap)

	if "simplytap.Card" != fmt.Sprint(reflect.TypeOf(card)) {
		t.Errorf("Instance expected:%s, got:%s", "simplytap.Card", string(fmt.Sprint(reflect.TypeOf(card))))
	}
}

func testCardCreate(t *testing.T, simplyTap SimplyTap, user User, wallet Wallet) Card {
	if card := getCreatedCard(); card.AccessToken != "" {
		return card
	}

	card := NewCardResource(simplyTap)
	card.SetWalletTokens(wallet)
	err := card.Create()

	if err != nil {
		t.Errorf("Exception occurred in method: %s, message: %s", "card.create", err)
	}

	createdCard = card

	return card
}

func TestCardCreate(t *testing.T) {
	simplyTap := GetConfig()

	user := testUserCreate(t, simplyTap)
	wallet := testWalletCreate(t, simplyTap, user)
	testCardCreate(t, simplyTap, user, wallet)
}

func TestCardDelete(t *testing.T) {
	card := getCreatedCard()
	err := card.Delete()

	if err != nil {
		t.Errorf("Exception occurred in method: %s, message: %s", "card.delete", err)
	}
}

func TestCardRespose(t *testing.T) {
	card := NewCardResource(GetConfig())
	var err string

	if err = card.Response(StatusNoCardBrandOrBadCredentials); err != StatusNoCardBrandOrBadCredentialsDescription {
		t.Errorf("Expected %s, got: %s", err, StatusNoCardBrandOrBadCredentialsDescription)
	}

	if err = card.Response(StatusBadAccessTokenSecret); err != StatusBadAccessTokenSecretDescription {
		t.Errorf("Expected %s, got: %s", err, StatusBadAccessTokenSecretDescription)
	}

	if err = card.Response(StatusCardCreationError); err != StatusCardCreationErrorDescription {
		t.Errorf("Expected %s, got: %s", err, StatusCardCreationErrorDescription)
	}
}

func TestCardSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "0"}`))

	card := NewCardResource(simplyTap)
	err := card.Send()

	if err != nil {
		t.Errorf("Exception occurred in method: %s, message: %s", "wallet.Send", err)
	}
}

func TestCardNoCardBrandOrBadCredentialSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "1"}`))

	card := NewCardResource(simplyTap)
	err := card.Send()

	if fmt.Sprintf("%s", err) != StatusNoCardBrandOrBadCredentialsDescription {
		t.Errorf("Expected %s, got: %s", err, StatusNoCardBrandOrBadCredentialsDescription)
	}
}

func TestCardBadAccessTokenSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "2"}`))

	card := NewCardResource(simplyTap)
	err := card.Send()

	if fmt.Sprintf("%s", err) != StatusBadAccessTokenSecretDescription {
		t.Errorf("Expected %s, got: %s", err, StatusBadAccessTokenSecretDescription)
	}
}

func TestCardCardCreationErrorSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "3"}`))

	card := NewCardResource(simplyTap)
	err := card.Send()

	if fmt.Sprintf("%s", err) != StatusCardCreationErrorDescription {
		t.Errorf("Expected %s, got: %s", err, StatusCardCreationErrorDescription)
	}
}
