package simplytap

import (
	"math/rand"
	"time"
)

const (
	// PasswordLength max length for password
	PasswordLength = 40
)

// Random represent random generator
type Random struct {
	Max int
	Min int
}

// Int Generates as random number from min to max
func (r Random) Int() int {
	rand.Seed(time.Now().UTC().UnixNano())
	return r.Min + rand.Intn(r.Max-r.Min)
}

// Str generate a random string
func (r Random) Str() string {
	rand.Seed(time.Now().UnixNano())
	var letterRunes = []rune("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ")
	b := make([]rune, r.Max)
	for i := range b {
		b[i] = letterRunes[rand.Intn(len(letterRunes))]
	}
	return string(b)
}
