package simplytap

import (
	"fmt"
	"net/http"
	"reflect"
	"testing"
	"time"

	"github.com/jarcoal/httpmock"
)

var createdUser User

func getCreatedUser() User {
	return createdUser
}

func TestNewUserResource(t *testing.T) {
	simplyTap := GetConfig()
	user := NewUserResource(simplyTap)

	if "simplytap.User" != fmt.Sprint(reflect.TypeOf(user)) {
		t.Errorf("Instance expected:%s, got:%s", "simplytap.User", string(fmt.Sprint(reflect.TypeOf(user))))
	}
}

func testUserCreate(t *testing.T, simplyTap SimplyTap) User {
	if user := getCreatedUser(); user.Email != "" {
		return user
	}

	user := NewUserResource(simplyTap)
	err := user.Create(testEmail())

	if err != nil {
		t.Errorf("Exception occurred in method: %s, message: %s", "user.create", err)
	}

	createdUser = user

	return user
}

func TestUserCreate(t *testing.T) {
	simplyTap := GetConfig()

	testUserCreate(t, simplyTap)
}

func TestUserUpdate(t *testing.T) {
	simplyTap := GetConfig()

	user := testUserCreate(t, simplyTap)
	err := user.Update(testEmail(), testPassword())

	if err != nil {
		t.Errorf("Exception occurred in method: %s, message: %s", "user.create", err)
	}
}

func TestUserRespose(t *testing.T) {
	user := NewUserResource(GetConfig())
	var err string

	if err = user.Response(StatusUserAlreadyExist); err != StatusUserAlreadyExistDescription {
		t.Errorf("Expected %s, got: %s", err, StatusUserAlreadyExistDescription)
	}

	if err = user.Response(StatusInvalidEmail); err != StatusInvalidEmailDescription {
		t.Errorf("Expected %s, got: %s", err, StatusInvalidEmailDescription)
	}

	if err = user.Response(StatusInvalidPassword); err != StatusInvalidPasswordDescription {
		t.Errorf("Expected %s, got: %s", err, StatusInvalidPasswordDescription)
	}

	if err = user.Response(StatusInsertUpdateError); err != StatusInsertUpdateErrorDescription {
		t.Errorf("Expected %s, got: %s", err, StatusInsertUpdateErrorDescription)
	}

	if err = user.Response(StatusAuthenticationError); err != StatusAuthenticationErrorDescription {
		t.Errorf("Expected %s, got: %s", err, StatusAuthenticationErrorDescription)
	}

	if err = user.Response("9"); err != StatusUncaughtExceptionDescription {
		t.Errorf("Expected %s, got: %s", err, StatusUncaughtExceptionDescription)
	}
}

func TestUserSend(t *testing.T) {
	simplyTap := GetConfig()

	user := NewUserResource(simplyTap)
	err := user.Send()

	if err != nil {
		t.Errorf("Exception occurred in method: %s, message: %s", "user.Send", err)
	}
}

func TestUserAlreadyExistSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "1"}`))

	user := NewUserResource(simplyTap)
	err := user.Send()

	if fmt.Sprintf("%s", err) != StatusUserAlreadyExistDescription {
		t.Errorf("Expected %s, got: %s", err, StatusUserAlreadyExistDescription)
	}
}

func TestUserInvalidEmailSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "2"}`))

	user := NewUserResource(simplyTap)
	err := user.Send()

	if fmt.Sprintf("%s", err) != StatusInvalidEmailDescription {
		t.Errorf("Expected %s, got: %s", err, StatusInvalidEmailDescription)
	}
}

func TestUserInvalidPasswordSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "3"}`))

	user := NewUserResource(simplyTap)
	err := user.Send()

	if fmt.Sprintf("%s", err) != StatusInvalidPasswordDescription {
		t.Errorf("Expected %s, got: %s", err, StatusInvalidPasswordDescription)
	}
}

func TestUserInsertUpdateErrorSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "4"}`))

	user := NewUserResource(simplyTap)
	err := user.Send()

	if fmt.Sprintf("%s", err) != StatusInsertUpdateErrorDescription {
		t.Errorf("Expected %s, got: %s", err, StatusInsertUpdateErrorDescription)
	}
}

func TestUserAuthenticationErrorSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "5"}`))

	user := NewUserResource(simplyTap)
	err := user.Send()

	if fmt.Sprintf("%s", err) != StatusAuthenticationErrorDescription {
		t.Errorf("Expected %s, got: %s", err, StatusAuthenticationErrorDescription)
	}
}

func testPassword() string {
	random := Random{Max: 10, Min: 8}
	return random.Str()
}

func testEmail() string {
	return fmt.Sprintf("test%d@matchmove.com", time.Now().Unix())
}
