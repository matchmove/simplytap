package simplytap

import (
	"fmt"
	"net/http"
	"reflect"
	"testing"

	"github.com/jarcoal/httpmock"
)

var createdWallet Wallet

func getCreatedWallet() Wallet {
	return createdWallet
}

func TestNewWalletResource(t *testing.T) {
	simplyTap := GetConfig()
	wallet := NewWalletResource(simplyTap)

	if "simplytap.Wallet" != fmt.Sprint(reflect.TypeOf(wallet)) {
		t.Errorf("Instance expected:%s, got:%s", "simplytap.Wallet", string(fmt.Sprint(reflect.TypeOf(wallet))))
	}
}

func testWalletCreate(t *testing.T, simplyTap SimplyTap, user User) Wallet {
	if wallet := getCreatedWallet(); wallet.AccessToken != "" {
		return wallet
	}

	wallet := NewWalletResource(simplyTap)
	wallet.SetUser(user)
	err := wallet.Create()

	if err != nil {
		t.Errorf("Exception occurred in method: %s, message: %s", "wallet.create", err)
	}

	createdWallet = wallet

	return wallet
}

func TestWalletCreate(t *testing.T) {
	simplyTap := GetConfig()

	user := testUserCreate(t, simplyTap)
	testWalletCreate(t, simplyTap, user)
}

func TestWalletDelete(t *testing.T) {
	simplyTap := GetConfig()

	user := testUserCreate(t, simplyTap)
	wallet := testWalletCreate(t, simplyTap, user)
	wallet.SetUser(user)
	err := wallet.Delete()

	if err != nil {
		t.Errorf("Exception occurred in method: %s, message: %s", "wallet.delete", err)
	}
}

func TestWalletRespose(t *testing.T) {
	wallet := NewWalletResource(GetConfig())
	var err string

	if err = wallet.Response(StatusNoUser); err != StatusNoUserDescription {
		t.Errorf("Expected %s, got: %s", err, StatusNoUserDescription)
	}

	if err = wallet.Response(StatusNoMobileApp); err != StatusNoMobileAppDescription {
		t.Errorf("Expected %s, got: %s", err, StatusNoMobileAppDescription)
	}

	if err = wallet.Response(StatusErrorInCreatingToken); err != StatusErrorInCreatingDescription {
		t.Errorf("Expected %s, got: %s", err, StatusErrorInCreatingDescription)
	}
}

func TestWalletSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "0"}`))

	wallet := NewWalletResource(simplyTap)
	err := wallet.Send()

	if err != nil {
		t.Errorf("Exception occurred in method: %s, message: %s", "wallet.Send", err)
	}
}

func TestWalletNoUserSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "1"}`))

	wallet := NewWalletResource(simplyTap)
	err := wallet.Send()

	if fmt.Sprintf("%s", err) != StatusNoUserDescription {
		t.Errorf("Expected %s, got: %s", err, StatusNoUserDescription)
	}
}

func TestWalletNoMobileSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "2"}`))

	wallet := NewWalletResource(simplyTap)
	err := wallet.Send()

	if fmt.Sprintf("%s", err) != StatusNoMobileAppDescription {
		t.Errorf("Expected %s, got: %s", err, StatusNoMobileAppDescription)
	}
}

func TestWalletErrorInCreatingSend(t *testing.T) {
	simplyTap := GetConfig()

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	httpmock.RegisterResponder(http.MethodPost, simplyTap.Host+"accounts/AdminApi",
		httpmock.NewStringResponder(200, `{"status": "3"}`))

	wallet := NewWalletResource(simplyTap)
	err := wallet.Send()

	if fmt.Sprintf("%s", err) != StatusErrorInCreatingDescription {
		t.Errorf("Expected %s, got: %s", err, StatusErrorInCreatingDescription)
	}
}
