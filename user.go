package simplytap

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"regexp"
	"time"
)

// User represent the user to be process
type User struct {
	SimplyTap   SimplyTap `json:"-"`
	Command     string    `json:"command,omitempty"`
	Email       string    `json:"email,omitempty"`
	Password    string    `json:"pass,omitempty"`
	NewEmail    string    `json:"new_email,omitempty"`
	NewPassword string    `json:"new_pass,omitempty"`
	Remove      string    `json:"remove,omitempty"`
}

// UserResponse represent the user response
type UserResponse struct {
	Status string `json:"status"`
}

const (
	// CommandCreateUser Command to create a user
	CommandCreateUser = "CreateUser"

	// CommandUpdateUser Command to create a user
	CommandUpdateUser = "UpdateUser"
)

// Posible status values for user create and update
const (
	StatusSuccess             = "0"
	StatusUserAlreadyExist    = "1"
	StatusInvalidEmail        = "2"
	StatusInvalidPassword     = "3"
	StatusInsertUpdateError   = "4"
	StatusAuthenticationError = "5"

	StatusUserAlreadyExistDescription    = "User already exist"
	StatusInvalidEmailDescription        = "Invalid email address"
	StatusRequiredEmailDescription       = "Email address is required"
	StatusRegexEmailDescription          = "Email address field must comply to RFC 5322 electronic mail address standards."
	StatusInvalidPasswordDescription     = "Invalid password"
	StatusRequiredPasswordDescription    = "Password is required"
	StatusInsertUpdateErrorDescription   = "Failed to insert or Update user record"
	StatusAuthenticationErrorDescription = "Authentication error"
	StatusUncaughtExceptionDescription   = "Uncaught exception"

	RegexEmail = `(?:[a-z0-9!#$%&'*+/=?^_` + "`" + `{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_` + "`" + `{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])`
)

// NewUserResource initializes the User Resource
func NewUserResource(simplyTap SimplyTap) User {
	return User{
		SimplyTap: simplyTap,
	}
}

// ValidateEmail validate email
func ValidateEmail(email string) error {
	re := regexp.MustCompile(RegexEmail)

	if !re.MatchString(email) {
		return errors.New(StatusRegexEmailDescription + " email:" + email)
	}

	return nil
}

func (u *User) validateCreate() error {
	if u.Email == "" {
		return errors.New(StatusRequiredEmailDescription)
	}

	if u.Password == "" {
		return errors.New(StatusRequiredPasswordDescription)
	}

	return ValidateEmail(u.Email)
}

func (u *User) validateUpdate() error {
	if u.NewEmail == "" {
		return errors.New(StatusRequiredEmailDescription)
	}

	if u.NewPassword == "" {
		return errors.New(StatusRequiredPasswordDescription)
	}

	return ValidateEmail(u.NewEmail)
}

func (u User) generatePassword() string {
	intPasswordLength := Random{Max: 20, Min: 10}.Int()

	rand := Random{
		Max: intPasswordLength,
	}

	return rand.Str()
}

// Create create user
func (u *User) Create(email string) error {
	rand := Random{Max: 9999, Min: 1000}
	token := fmt.Sprintf("%s_%d_%d", email, time.Now().Unix(), rand.Int())
	hash := Hash{token}.MD5()
	u.Command = CommandCreateUser
	u.Email = hash + "@matchmove.com"
	u.Password = Hash{hash[0:10]}.Encrypt()
	if errCreate := u.validateCreate(); errCreate != nil {
		return errCreate
	}
	return u.Send()
}

// SetUser set user data
func (u *User) SetUser(email string, password string, remove string) error {
	u.Email = email
	u.Password = password
	u.Remove = remove
	return nil
}

// Update update user
func (u *User) Update(email string, password string) error {

	u.Command = CommandUpdateUser
	u.NewEmail = email
	u.NewPassword = password

	if errCreate := u.validateCreate(); errCreate != nil {
		return errCreate
	}

	if errUpdate := u.validateUpdate(); errUpdate != nil {
		return errUpdate
	}

	return u.Send()
}

// Response get the description for the response code
func (u User) Response(code string) string {
	switch code {
	case StatusUserAlreadyExist:
		return StatusUserAlreadyExistDescription
	case StatusInvalidEmail:
		return StatusInvalidEmailDescription
	case StatusInvalidPassword:
		return StatusInvalidPasswordDescription
	case StatusInsertUpdateError:
		return StatusInsertUpdateErrorDescription
	case StatusAuthenticationError:
		return StatusAuthenticationErrorDescription
	}
	return StatusUncaughtExceptionDescription
}

// Send send the request to simplytap
func (u *User) Send() error {
	jsonString, _ := json.Marshal(u)
	data := fmt.Sprintf("DATA=%s", string(jsonString))
	_, body := u.SimplyTap.Send(http.MethodPost, u.SimplyTap.Host+"accounts/AdminApi", data, u.SimplyTap.Issuer.Key, u.SimplyTap.Issuer.Secret)

	response := UserResponse{}
	err := json.Unmarshal([]byte(string(body)), &response)

	if err != nil {
		return err
	}

	if response.Status == StatusSuccess {
		return nil
	}

	return errors.New(u.Response(response.Status))
}
