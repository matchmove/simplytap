package simplytap

import (
	"flag"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"

	"bitbucket.org/matchmove/rest"
)

var mockRequestEnabled string
var configPathArg string
var userIDPathArg string

// init handles the -mock and --mock argument
func init() {
	flag.StringVar(&userIDPathArg, "user_id", "", "YAML configuration file location. Default ``")
	flag.StringVar(&mockRequestEnabled, "mock", "true", "YAML configuration file location. Default `true`")
	flag.StringVar(&configPathArg, "config", "", "YAML configuration file location. Default `hce.yml`")
	flag.Parse()
}

const (
	//ContentType header definition
	ContentType = "Content-Type"
)

// SimplyTap represents the information about the SimplyTap.
type SimplyTap struct {
	Host      string
	Issuer    Credential
	Developer Credential
	CardBrand string
}

// Credential contains the key and secret
type Credential struct {
	Key    string
	Secret string
}

var simplyTap SimplyTap

// GetConfigFile returns the file to be loaded as a configuration source
func GetConfigFile() string {
	var confPath string

	if idx := strings.LastIndex(configPathArg, "."); idx != -1 {
		confPath = configPathArg[0:idx]
	} else {
		confPath = configPathArg
	}

	return confPath
}

// GetUserID mock request enabled
func GetUserID() string {
	return userIDPathArg
}

// GetHTTPMock mock request enabled
func GetHTTPMock() string {
	return mockRequestEnabled
}

// GetConfig get simplytap config
func GetConfig() SimplyTap {
	if "" == GetConfigFile() {
		simplyTap.InitFromEnv()
		log.Println("Configuration loaded from environment variables.")
	} else if err := rest.LoadConfig(GetConfigFile(), &simplyTap); err != nil {
		log.Fatalf("Configuration cannot be loaded with error `%v`", err)
	}

	return simplyTap
}

// InitFromEnv load simplytap config from environment variable
func (s *SimplyTap) InitFromEnv() {
	EnviromentVariables := []string{
		"SIMPLYTAPP_HOST",
		"SIMPLYTAPP_ISSUER_KEY",
		"SIMPLYTAPP_ISSUER_SECRET",
		"SIMPLYTAPP_DEVELOPER_KEY",
		"SIMPLYTAPP_DEVELOPER_SECRET",
		"SIMPLYTAPP_CARD_BRAND",
	}

	for _, env := range EnviromentVariables {
		if os.Getenv(env) == "" {
			log.Fatalf("Configuration cannot be loaded, Environment variable %s is missing.", env)
		}
	}

	s.Host = os.Getenv("SIMPLYTAPP_HOST")

	s.Issuer = Credential{
		Key:    os.Getenv("SIMPLYTAPP_ISSUER_KEY"),
		Secret: os.Getenv("SIMPLYTAPP_ISSUER_SECRET"),
	}

	s.Developer = Credential{
		Key:    os.Getenv("SIMPLYTAPP_DEVELOPER_KEY"),
		Secret: os.Getenv("SIMPLYTAPP_DEVELOPER_SECRET"),
	}

	s.CardBrand = os.Getenv("SIMPLYTAPP_CARD_BRAND")
}

// Send send request
func (s SimplyTap) Send(method string, uri string, data string, username string, password string) (*http.Response, string) {

	req, err := http.NewRequest(http.MethodPost, uri, strings.NewReader(data))
	if err != nil {
		panic(err)
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		panic(err)
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		panic(err)
	}

	return resp, string(body)
}
