package simplytap

import "testing"

func TestRandomString(t *testing.T) {
	rand := Random{
		Max: PasswordLength,
	}
	randomString := rand.Str()
	if len(randomString) != 40 {
		t.Errorf("Length was `%v`, expecting `%v`", len(randomString), 40)
	}
}

func TestRandomInteger(t *testing.T) {
	rand := Random{
		Max: PasswordLength,
	}
	randomInt := rand.Int()
	if randomInt > 40 {
		t.Errorf("Value was `%v`, expecting value greater than `%v` but less than `%v`", randomInt, rand.Min, rand.Max)
	}
}
