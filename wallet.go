package simplytap

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
)

// Wallet represent the wallet
type Wallet struct {
	SimplyTap   SimplyTap `json:"-"`
	Command     string    `json:"command,omitempty"`
	Email       string    `json:"email,omitempty"`
	Password    string    `json:"pass,omitempty"`
	Key         string    `json:"consumer_key,omitempty"`
	Secret      string    `json:"consumer_secret,omitempty"`
	AccessToken string    `json:"access_token,omitempty"`
	TokenSecret string    `json:"token_secret,omitempty"`
}

// DataResponse response for wallet create
type DataResponse struct {
	CardID      string `json:"card_id,omitempty"`
	AccessToken string `json:"access_token,omitempty"`
	TokenSecret string `json:"token_secret,omitempty"`
	AgentHash   string `json:"agent_hash,omitempty"`
	SpecVersion string `json:"spec_ver,omitempty"`
}

// WalletResponse represent the wallet response
type WalletResponse struct {
	Data   DataResponse
	Status string `json:"status,omitempty"`
}

// NewWalletResource initializes the User Resource
func NewWalletResource(simplyTap SimplyTap) Wallet {
	return Wallet{
		SimplyTap: simplyTap,
	}
}

const (
	// CommandCreateWallet Command to create a user
	CommandCreateWallet = "CreateAppToken"

	// CommandDeleteWallet Command to create a user
	CommandDeleteWallet = "UnregisterApp"
)

// Posible status values for user create and update
const (
	StatusNoUser               = "1"
	StatusNoMobileApp          = "2"
	StatusErrorInCreatingToken = "3"

	StatusNoUserDescription          = "No user found"
	StatusNoMobileAppDescription     = "No mobile app found"
	StatusErrorInCreatingDescription = "Error creating wallet"
	StatusRequiredKeyDescription     = "Key is required"
	StatusRequiredSecretDescription  = "Secret is required"
)

func (w *Wallet) validateCreate() error {
	if w.Email == "" {
		return errors.New(StatusInvalidEmailDescription)
	}

	if w.Password == "" {
		return errors.New(StatusInvalidPasswordDescription)
	}

	return ValidateEmail(w.Email)
}

// Create create Wallet
func (w *Wallet) Create() error {
	w.Command = CommandCreateWallet
	w.Key = w.SimplyTap.Developer.Key
	w.Secret = w.SimplyTap.Developer.Secret

	if err := w.validateCreate(); err != nil {
		return err
	}

	return w.Send()
}

// SetToken set token values
func (w *Wallet) SetToken(token string, secret string) error {
	w.AccessToken = token
	w.TokenSecret = secret
	return nil
}

// SetUser set value Wallet
func (w *Wallet) SetUser(user User) error {
	w.Email = user.Email
	w.Password = user.Password
	return nil
}

// Delete unregister wallet
func (w *Wallet) Delete() error {
	wallet := NewWalletResource(w.SimplyTap)
	wallet.Command = CommandDeleteWallet
	wallet.AccessToken = w.AccessToken
	wallet.TokenSecret = w.TokenSecret
	return wallet.Send()
}

// Response get the description for the response code
func (w Wallet) Response(code string) string {
	switch code {
	case StatusNoUser:
		return StatusNoUserDescription
	case StatusNoMobileApp:
		return StatusNoMobileAppDescription
	case StatusErrorInCreatingToken:
		return StatusErrorInCreatingDescription
	}
	return StatusUncaughtExceptionDescription
}

// Send send the request to simplytap
func (w *Wallet) Send() error {
	jsonString, _ := json.Marshal(w)
	data := fmt.Sprintf("DATA=%s", string(jsonString))
	_, body := w.SimplyTap.Send(http.MethodPost, w.SimplyTap.Host+"accounts/AdminApi", data, w.SimplyTap.Developer.Key, w.SimplyTap.Developer.Secret)

	response := WalletResponse{}
	err := json.Unmarshal([]byte(string(body)), &response)

	if response.Data.AccessToken != "" && response.Data.TokenSecret != "" {
		w.SetToken(response.Data.AccessToken, response.Data.TokenSecret)
	}

	if err != nil {
		return err
	}

	if response.Status == StatusSuccess {
		return nil
	}

	return errors.New(w.Response(response.Status))
}
